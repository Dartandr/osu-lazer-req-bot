const fetch = require('node-fetch');
const rp = require('request-promise');
const readlineSync = require('readline-sync');
const TwitchBot = require('twitch-bot');

let nick = readlineSync.question("nickname \n");
let password = readlineSync.question("pass \n");
let twitchChannel = readlineSync.question("twitch channel \n");
let target_id = readlineSync.question("target_id \n");
let cfduid;
let session;
let xsrf;
let twitchToken = '';
let OsuApiKey = '';


const message = (message) => {
    rp(`https://osu.ppy.sh/community/chat/channels/${target_id}/messages`, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
            "Cookie": `__cfduid=${cfduid}; osu_session=${session}; _encid=%7B%22name%22%3A%22${nick}%22%2C%22email%22%3A%22%22%2C%22_source%22%3A%22a%22%7D; XSRF-TOKEN=${xsrf}`,
            'X-CSRF-Token': xsrf
        },
        body: {
            "is_action": false,
            "message": `${message}`,
            "target_id": target_id,
            "target_type": "channel"
        },
        json: true,
        resolveWithFullResponse: true
    })
        .then(res => { })
}


const Main = () => {
    let a;
    fetch('https://osu.ppy.sh/home')
        .then(res => res.headers.get('set-cookie'))
        .then(data => {
            a = data.split(';')[4];
            let xsrfTokenFirs = a.split(' ')[2];
            xsrfTokenFirs = xsrfTokenFirs.slice(11);
            cfduid = data.split(';')[0];
            cfduid = cfduid.slice(9);
            let sessionOsu = data.split(';')[8].split(' ')[2]
            sessionOsu = sessionOsu.slice(12);
            //console.log(xsrfTokenFirs)
            //console.log(sessionOsu+'\n'+xsrfTokenFirs+'\n'+cfduid);
            rp('https://osu.ppy.sh/session', {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Cookie": `__cfduid=${cfduid}; osu_session=${sessionOsu}; _encid=%7B%22name%22%3A%22${nick}%22%2C%22email%22%3A%22%22%2C%22_source%22%3A%22a%22%7D; XSRF-TOKEN=${xsrfTokenFirs}`
                },
                body: {
                    "_token": xsrfTokenFirs,
                    "username": nick,
                    "password": password
                },
                json: true,
                resolveWithFullResponse: true
            })
                .then(res => {
                    //console.log(res.headers['set-cookie'])
                    let data = res.headers['set-cookie']
                    session = data[1].split(';')[0].slice(12)
                    xsrf = data[0].split(';')[0].slice(11);
                    //console.log(data)
                    //console.log(`__cfduid=${cfduid}; osu_session=${session}; _encid=%7B%22name%22%3A%22${nick}%22%2C%22email%22%3A%22%22%2C%22_source%22%3A%22a%22%7D; XSRF-TOKEN=${xsrf}`)
                });
        })
}

Main();


const Bot = new TwitchBot({
    username: 'Dartandr',
    oauth: `oauth:${twitchToken}`,
    channels: [twitchChannel]
})

Bot.on('join', channel => {
    console.log(`Joined channel: ${channel}`)
    Bot.say('Hey ho lazer req is now working!')
})

Bot.on('error', err => {
    console.log(err)
})

Bot.on('message', chatter => {
    if (chatter.message.includes('https://osu.ppy.sh/')) {
        for (let i = 0; i < 1; i++) {
            let id
            if (chatter.message.includes('https://osu.ppy.sh/beatmapsets/')) {
                id = chatter.message.split('/')[5]
            } else if (chatter.message.includes('https://osu.ppy.sh/b/')) {
                id = chatter.message.split('/')[4].split('&')[0].split('?')[0]
            } else if (chatter.message.includes('https://osu.ppy.sh/s/')) {
                Bot.say(`Use a new osu site pls! || Используй новый сайт!!!!!!!!!!`)
                break;
            } else {
                break;
            }

            fetch(`https://osu.ppy.sh/api/get_beatmaps?k=${OsuApiKey}&b=${id}`)
                .then(res => res.json())
                .then(data => {
                    let messageData = `${chatter.username}: [https://osu.ppy.sh/b/${id} ${data[0].artist} - ${data[0].title}] | AR:${data[0].diff_approach} | SR:${data[0].difficultyrating}*`
                    message (messageData)
                    let status = '';
                    switch (data[0].approved){
                        case '1':
                            status = 'Ranked';
                            break;
                        case '2':
                            status = 'Approved';
                            break;
                        case '3':
                            status = 'Qualified';
                            break;
                        case '4':
                            status = 'Loved';
                            break;
                        case '0':
                            status = 'Pending';
                            break;
                        case '-1':
                            status = 'WIP';
                            break;
                        case '-2':
                            status = 'Graveyard';
                            break;
                    }
                    Bot.say(`[${status}] ${data[0].artist} - ${data[0].title} [${data[0].version}] (by ${data[0].creator}), ${data[0].bpm} BPM, ${Math.round(data[0].difficultyrating,2)} stars`)
                })

            
        }
    }

})
